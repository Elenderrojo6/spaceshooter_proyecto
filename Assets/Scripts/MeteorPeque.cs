﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorPeque : MonoBehaviour
{
    Vector2 speed;
    Transform graphics;
    
    public ParticleSystem ps;
    
    public AudioSource audioSource;
    
    void Awake()
    {
       graphics = transform.GetChild(0);

        for(int i=0; i<graphics.childCount;i++){
            graphics.GetChild(i).gameObject.SetActive(false);
        }
        int seleccionado=Random.Range(0,graphics.childCount);
        graphics.GetChild(seleccionado).gameObject.SetActive(true);

        speed.x= Random.Range(-1,-5);
        speed.y= Random.Range(5,-5);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed*Time.deltaTime);
        graphics.Rotate(0,0,Time.deltaTime*100);
    }
     public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }

        else if(other.tag == "Bala") {
            StartCoroutine(DestroyMeteor());
        }
    
     }
     IEnumerator DestroyMeteor(){
         
         graphics.gameObject.SetActive(false);
         
         Destroy(GetComponent<Collider2D>());

         ps.Play();

         audioSource.Play();

         yield return new WaitForSeconds(1.0f);

         Destroy(this.gameObject);
     }
}