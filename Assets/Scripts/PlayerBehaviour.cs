﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class PlayerBehaviour : MonoBehaviour
{    
    public float speed;
    private Vector2 axis;
    public Vector2 limits;
    public bool facingRight = true;
  
 
    private float shootTime=0;
 
    public Weapon weapon;
    public Weapon weapon1;

    public Propeller prop;
    [SerializeField] GameObject vidauno;
    [SerializeField] GameObject vidados;
    [SerializeField] GameObject vidatres;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
 
    
    public int lives = 3;
    private bool iamDead = false;
   


    // Update is called once per frame
    void Update () {

        if(iamDead){
            return;
        }

       

        shootTime += Time.deltaTime;
 
        transform.Translate (axis * speed * Time.deltaTime);
 
        if (transform.position.x > limits.x && facingRight) {
            Flip();
            transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
        }else if (transform.position.x < -limits.x && !facingRight) {
            Flip();
            transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
        }
 
        if (transform.position.y > limits.y) {
            transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
        }else if (transform.position.y < -limits.y) {
            transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
        }
 
        if(axis.x>0){
            prop.BlueFire();
        }else if(axis.x<0){
            prop.RedFire();
        }else{
            prop.Stop();
        }
    }
 
    public void SetAxis(Vector2 currentAxis){
        axis = currentAxis;
    }
 
    public void SetAxis(float x, float y){
        axis = new Vector2(x,y);
    }
 
    public void Shoot(){
        if(shootTime>weapon.GetCadencia()){
            shootTime = 0f;
            weapon.Shoot();
        }
    }
    public void Shoot1()
    {
        if (shootTime > weapon1.GetCadencia())
        {
            shootTime = 0f;
            weapon1.Shoot();
        }
    }

    void Flip (){
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180);
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Meteor" || other.tag == "Enemy" || other.tag == "Finish") {
            StartCoroutine(DestroyShip());
        }

        else if(other.tag == "Botiquin"){
            lives++;
        }
    }
 
    IEnumerator DestroyShip(){
        //Indico que estoy muerto
        iamDead=true;
 
        //Me quito una vida
        lives--;

        //Desactivo el grafico
        graphics.SetActive(false);
 
        //Elimino el BoxCollider2D
        collider.enabled = false;
 
        //Lanzo la partícula
        ps.Play();
 
        //Lanzo sonido de explosion
        audioSource.Play();
 
        //Desactivo el propeller
        prop.gameObject.SetActive(false);
 
        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

       
        //Miro si tengo mas vidas
        if(lives>0){
            StartCoroutine(ParpadeoShip());          
        }

        // Pasa a escena GameOver
        if(lives==0){
            SceneManager.LoadScene("die");
        }
        if (lives == 2) {
            vidatres.SetActive(false);
        }
        if (lives == 1)
        {
            vidados.SetActive(false);
        }
        if (lives == 0)
        {
            vidauno.SetActive(false);
        }
    }

    IEnumerator ParpadeoShip(){

        //Vuelvo a activar el jugador
        iamDead = false;
        graphics.SetActive(true);
        //Activo el propeller
        prop.gameObject.SetActive(true);
         graphics.SetActive(true);
         yield return new WaitForSeconds(0.5f);
         graphics.SetActive(false);
         yield return new WaitForSeconds(0.5f);
         graphics.SetActive(true);
         graphics.SetActive(true);
         yield return new WaitForSeconds(0.5f);
         graphics.SetActive(false);
         yield return new WaitForSeconds(0.5f);
         graphics.SetActive(true);
         collider.enabled = true;
    }




 
}