﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Enemy : MonoBehaviour
{
    

    [SerializeField] GameObject[] sprites;
    private int elegido;
 
    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
 
    private float timeCounter;
    private float timeToShoot;
 
    private float timeShooting;
    private float speedx;
    private score Skore;
 
    private bool isShooting;
 
    [SerializeField] GameObject bullet;

   

    public  void Awake() {
        Skore = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<score>();
        elegido = Random.Range(0,sprites.Length);
 
        for(int kk=0;kk<sprites.Length;kk++){
            sprites[kk].SetActive(false);
        }
 
        sprites[elegido].SetActive(true);
 
        Inicitialization();
    }
 
    protected virtual void Inicitialization(){
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }
 
    protected virtual void EnemyBehaviour(){
        timeCounter += Time.deltaTime;
 
        if(timeCounter>timeToShoot){
            if(!isShooting){
                isShooting = true;
                Instantiate(bullet,this.transform.position,Quaternion.Euler(0,0,180),null);
            }
            if(timeCounter>(timeToShoot+timeShooting)){
                timeCounter = 0.0f;
                isShooting = false;
            }
        }else{
            transform.Translate(-speedx*Time.deltaTime,0,0);
        }
 
    }
 
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehaviour();
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        if(other.tag == "Bala") {
            StartCoroutine(DestroyShip());
        }

        else if(other.tag == "Finish"){
            StartCoroutine(DestroyShip());
        }
    }
 
 
    IEnumerator DestroyShip(){

        sprites[elegido].SetActive(false);
 
        collider.enabled = false;
 
        ps.Play();

        audioSource.Play();

        yield return new WaitForSeconds(1.0f);
            
        Skore.AddScore(50);
        Destroy(this.gameObject);
    }
}