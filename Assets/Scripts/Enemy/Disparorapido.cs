﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparorapido : MonoBehaviour
{
    public int life = 2;
    [SerializeField] GameObject[] sprites;
    private int elegido;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;
    private score Skore;

    private bool isShooting;

    [SerializeField] GameObject bullet;

    private void Awake()
    {
        Skore = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<score>();
        elegido = Random.Range(0, sprites.Length);

        for (int kk = 0; kk < sprites.Length; kk++)
        {
            sprites[kk].SetActive(false);
        }

        sprites[elegido].SetActive(true);

        Inicitialization();
    }


    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 0.2f;
        timeShooting = 0.5f;
        speedx = 3.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;

        if (timeCounter > timeToShoot)
        {
            if (!isShooting)
            {
                isShooting = true;
                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        else
        {
            transform.Translate(-speedx * Time.deltaTime, 0, 0);
        }

    }
    
    void Update()
    {
        EnemyBehaviour();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bala")
        {
            if (life < 0){
                StartCoroutine(DestroyShip());
            }
            else{
                life--;
            }
        }

        else if (other.tag == "Finish")
        {
            StartCoroutine(DestroyShip());
        }
    }


    IEnumerator DestroyShip()
    {
        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Skore.AddScore(200);
        Destroy(this.gameObject);
    }
}