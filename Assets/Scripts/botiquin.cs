﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botiquin : MonoBehaviour
{

    [SerializeField] GameObject[] sprites;
    private int elegido;

    [SerializeField] BoxCollider2D collider;
    [SerializeField] AudioSource audioSource;

    private float speedx;

    private void Awake()
    {
    
        elegido = Random.Range(0, sprites.Length);

        for (int kk = 0; kk < sprites.Length; kk++)
        {
            sprites[kk].SetActive(false);
        }

        sprites[elegido].SetActive(true);

        Inicitialization();
    }

      protected virtual void Inicitialization()
    {
        speedx = 3.0f;
    }

 protected virtual void EnemyBehaviour()
    {
    
            transform.Translate(-speedx * Time.deltaTime, 0, 0);
    }

     void Update()
    {
        EnemyBehaviour();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "Finish" || other.tag == "Ship"  )
        {
            StartCoroutine(DestroyBotiquin());
        }
    }


    IEnumerator DestroyBotiquin()
    {
        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }

}
