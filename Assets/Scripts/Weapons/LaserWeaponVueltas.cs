﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserWeaponVueltas : Weapon
{

    public GameObject VueltasBullet;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(VueltasBullet, this.transform.position, Quaternion.identity, null);
    }

}