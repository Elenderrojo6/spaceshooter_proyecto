﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class VueltasBullet : MonoBehaviour
{
    public float speed;
    public float timeVueltas;
 
    private float currentTimeVueltas = 0;
 
   
    // Update is called once per frame
    void Update () {
        currentTimeVueltas += Time.deltaTime;
        if(currentTimeVueltas < timeVueltas)
        {
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeVueltas> timeVueltas && currentTimeVueltas < timeVueltas * 2){
            transform.Translate (0, -speed * Time.deltaTime,0);        
        }else if(currentTimeVueltas> timeVueltas* 2 && currentTimeVueltas< timeVueltas* 3){
            transform.Translate (-speed * Time.deltaTime,0, 0);
        }else if(currentTimeVueltas> timeVueltas* 3 && currentTimeVueltas< timeVueltas* 5){
            transform.Translate (0, speed * Time.deltaTime,0);
        }else if(currentTimeVueltas> timeVueltas* 5 && currentTimeVueltas< timeVueltas* 6){
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeVueltas > timeVueltas * 6 && currentTimeVueltas < timeVueltas* 7){
            transform.Translate (0, -speed * Time.deltaTime,0);
        }else{
            currentTimeVueltas = 0;
        }
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        Debug.Log("Soy trigger");
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }
 
}