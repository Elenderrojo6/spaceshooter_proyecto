﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
   public void PulsaPlay(){
       SceneManager.LoadScene("game_1");
   }
   public void PulsaExit(){
       Application.Quit();
   }
    public void PulsaCreditos(){
        SceneManager.LoadScene("Credits");
    }
}
